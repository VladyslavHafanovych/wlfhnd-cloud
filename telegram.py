import http.client
import json
import os

conn = http.client.HTTPSConnection(os.getenv('API_HOST'))
api_key = os.getenv('API_KEY')
endpoint = f"/bot{api_key}/"

if api_key is None:
    raise EnvironmentError("Missing API_KEY env variable!")
    

def send_message(message):
    global endpoint
    method_endpoint = f"{endpoint}sendMessage"

    payload = {
        'chat_id': -1001664881484,
        'text': f"{message}"
    }

    headers = {'content-type': "application/json"}

    conn.request("POST", endpoint, json.dumps(payload), headers)

    res = conn.getresponse()

    return {
        'statusCode': res.status,
        'body': json.dumps('Message sent')
    }

